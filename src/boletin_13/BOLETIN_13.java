package boletin_13;
/**
 *
 * @author galego
 */

import java.util.*;
import javax.swing.*;

public class BOLETIN_13 {

    public static void main(String[] args) {
        
        Buzon objBuzon = new Buzon();
        byte lido=1;
        byte nonlido=0;
        
        String opc;
        int opcion=1;
        while (opcion!=7) {
            opc = JOptionPane.showInputDialog("¿Qué accion desexa realizar?"
                    + "\n 1. Visualizar o número de correos da súa bandexa"
                    + "\n 2. Engadir un correo"
                    + "\n 3. Visualizar se quedan correos por ler"
                    + "\n 4. Visualizar o primer correo non lido"
                    + "\n 5. Visulaizar un correo"
                    + "\n 6. Eliminar un correo"
                    + "\n 7. Fin aplicación");
        
            opcion = Integer.parseInt(opc);
            
            switch (opcion){
                
                case 1:
                    objBuzon.numCorreos();
                    break;
                    
                case 2:
                    Scanner resposta = new Scanner(System.in);
                    System.out.println("Introduce o texto do correo");
                    String correo1 = resposta.nextLine();
                    Correo correo = new Correo(correo1,nonlido);
                    objBuzon.engade(correo);
                    break;
                    
                case 3:
                    objBuzon.porLer();
                    break;
                    
                case 4:
                    objBuzon.PrimerNoLeido();
                    break;
                    
                case 5:
                    System.out.println("Introduce o número do correo que desexa visualizar");
                    Scanner resposta2 = new Scanner(System.in);
                    int k = resposta2.nextInt();
                    objBuzon.amosa(k);
                    break;
                    
                case 6:
                    System.out.println("Introduce o número do correo que desexa eliminar");
                    Scanner resposta3 = new Scanner(System.in);
                    int k2 = resposta3.nextInt();
                    objBuzon.elimina(k2);
                    break;
                    
                 case 7:
                    break;
                
                 default:
                    System.out.println("Introduzca unha acción correcta");
            }
        
        }
    }
    
}