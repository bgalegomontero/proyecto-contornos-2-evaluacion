package boletin_13;
/**
 *
 * @author galego
 */
import java.util.*;
import java.util.Map.Entry;
import javax.swing.JOptionPane;

public class Buzon {
    byte lido=1;
    byte nonlido=0;
    
    TreeMap<String,Correo> treemap;
    
    public  Buzon(){
        treemap = new TreeMap(); 
    }
    
    public void numCorreos (){
        System.out.println("O numero de correos é: "+treemap.size());
    }
    
    public void engade(Correo correo){
        Correo text = new Correo(correo.getContido());
        Date timestamp = new Date();
        treemap.put(String.valueOf(timestamp.getTime()),text);
    }
    
    public void porLer(){
        Iterator i= treemap.entrySet().iterator();
        while (i.hasNext()){
            Map.Entry me=(Map.Entry)i.next();
            Correo correoaux =(Correo)me.getValue();
            if(correoaux.getLido()==0){
                System.out.println("Tes algún mensaxe sen leer");
                return;
            }
        }
        System.out.println("Non hay mensaxes sen leer");
    }
    
    public void PrimerNoLeido(){
        for(Map.Entry entrada : treemap.entrySet() ){
            Correo correo =(Correo)entrada.getValue();
            if(correo.getLido()==nonlido) {
                correo.setLido(lido);
                treemap.put ((String)entrada.getKey(), correo);
                System.out.println("O primer mensaxe non lido é: ");
                System.out.println(correo.getContido());
                return;
            }
        }
        System.out.println("Non tes ningún correo sen ler");
    }
    
    public void amosa(int k){
        int cont = 1;    

        if(k> treemap.size()|| treemap.size()<1){
            JOptionPane.showMessageDialog(null, "O número introducido non é o correcto. Tes " + treemap.size()+ " correos na túa bandexa");
        }
        else{
            for(Entry bandeja_entrada: treemap.entrySet()){
                if (cont==k){
                    System.out.println(treemap.get(bandeja_entrada.getKey()).getContido());
                    break;
                }
                cont ++;
            }      
        }
    }
        
    public void elimina(int k){
        int cont = 1;
        //boolean encontrado=false;
        if(k> treemap.size()|| treemap.size()<1){
            JOptionPane.showMessageDialog(null, "O número introducido non é o correcto. Tes " + treemap.size()+ " correos na túa bandexa");
        }
        
        for(Entry bandeja_entrada: treemap.entrySet()){
            if (cont==k){
                System.out.println("O correo: " + treemap.get(bandeja_entrada.getKey()).getContido()+". foi eliminado");
                treemap.remove(bandeja_entrada.getKey());
            }
            cont++;
        }
    }
}
    
    
