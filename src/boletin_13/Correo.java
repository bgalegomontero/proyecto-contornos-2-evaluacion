package boletin_13;

/**
 *
 * @author galego
 */
public class Correo {
    
    
    private String contido;
    private byte lido;
    
    
   public Correo(){  
      
   }
   public Correo(String contido,byte lido){
     
       this.contido = contido;
       this.lido = lido;
   }
   
   public Correo (String contido){
       
       this.contido = contido;
       lido = 0;
    }   
   
    public String getContido() {
        return contido;
    }
    
    public void setContido(String contido) {
        this.contido = contido;
   }

   public byte getLido() {
        return lido;
   }

   public void setLido(byte lido) {
        this.lido = lido;
   }
}